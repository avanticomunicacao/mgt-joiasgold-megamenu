<?php
namespace Ibnab\MegaMenu\Model\Category;

class DataProvider extends \Magento\Catalog\Model\Category\DataProvider
{
    protected function getFieldsMap()
    {
        $fields = parent::getFieldsMap();
        $fields['content'][] = 'icon_category'; // custom image field

        return $fields;
    }
}
