<?php

namespace Ibnab\MegaMenu\Block;

use Ibnab\MegaMenu\Helper\Category;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Image extends Template
{
    /**
     * @var Product
     */
    protected $_category = null;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var Category
     */
    protected $_categoryHelper;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Category $categoryHelper,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        $this->_categoryHelper = $categoryHelper;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve current category model object
     *
     * @return \Magento\Catalog\Model\Category
     */
    public function getCurrentCategory()
    {
        if (!$this->_category) {
            $this->_category = $this->_coreRegistry->registry('current_category');
            if (!$this->_category) {
                throw new LocalizedException(__('Category object could not be found in core registry'));
            }
        }
        return $this->_category;
    }
    public function getImageUrl()
    {
        $imageCode = $this->hasImageCode() ? $this->getImageCode() : 'image';
        $image = $this->getCurrentCategory()->getData($imageCode);
        return $this->_categoryHelper->getImageUrl($image);
    }
}
